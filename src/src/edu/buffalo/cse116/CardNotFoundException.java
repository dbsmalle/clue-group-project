package src.edu.buffalo.cse116;

public class CardNotFoundException extends Exception {
	

	private static final long serialVersionUID = 1L;

	
	/**
	*
	*CardNotFoundException- Thrown when a card can not be found.
	*
	*@param message - The message shown when this exception is thrown
	*
	*/
	
	
	public CardNotFoundException(String message){
		super(message);
	}

}
