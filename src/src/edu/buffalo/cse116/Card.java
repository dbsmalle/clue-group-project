package src.edu.buffalo.cse116;

import java.util.ArrayList;
import java.util.Random;

public class Card {
	
	
	/**
	*
	*An arraylist containing all character cards in the game.
	*
	*/
	
	public void characterList() {
        ArrayList<Object> Characters = new ArrayList<Object>();
        Characters.add("Miss Scarlett");
        Characters.add("Professor Plum");
        Characters.add("Mrs Peacock");
        Characters.add("Mr Green");
        Characters.add("ColMustard");
        Characters.add("Dr Orchid");
        
        
  //      randomGenerator = new Random();
  //       String index = randomGenerator.nextString(Characters.size());
  //      String random = list.get(randomizer.nextInt(list.size()));

  //	return values()[(int) (Math.random() * values().length)];
	}
	
	
	
	/**
	*
	*An arraylist containing all wepaon cards in the game.
	*
	*/
	public void weaponList(){
		ArrayList<Object> Weapons =  new ArrayList<Object>();
		Weapons.add("Candlestick");
		Weapons.add("Knife");
		Weapons.add("Lead Pipe");
		Weapons.add("Revolver");
		Weapons.add("Rope");
		Weapons.add("Wrench");
		
		}
	
	
	/**
	*
	*An arraylist containing all room cards in the game.
	*
	*/
	public void roomsList() {
		//		final String[] Rooms = { "Kitchen", "Ballroom", "Conservatory", "Dining Room", "Lounge", "Hall", "Study", "Library", "Billiard Room" };

		ArrayList<Object> Rooms = new ArrayList<Object>();
		Rooms.add("Kitchen");
		Rooms.add("Ballroom");
		Rooms.add("Conservatory");
		Rooms.add("Dining Room");
		Rooms.add("Lounge");
		Rooms.add("Hall");
		Rooms.add("Study");
		Rooms.add("Library");
		Rooms.add("Billiard Room");
	}
 
}
