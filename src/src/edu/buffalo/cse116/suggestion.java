package src.edu.buffalo.cse116;

import java.util.ArrayList;

public class suggestion {

private Card[] guess;
private int cursor;



/**
* Call disprove on players starting to the left of the player making the suggestion
* This should then return the player who will disprove the suggestion, as per the second user story. Note that disprove is a boolean method
*
* @param card A = Cards that reference character cards
* @param card B = Cards that reference Room cards
* @param card C = Cards that reference weapon cards
*
*
*/
	public suggestion(Card a, Card b, Card c){
		// player making a guess call suggestion with their three guesses as parameters
		Card[] suggest = new Card[3];
		suggest[0] = a;
		suggest [1] = b;
		suggest [2] = c;
		guess = suggest;

		
		
		
	}
	
	
	/**
	*Determines a guess by taking a players hand and checks it against the guessed card.
	*@param p = A reference to a player object
	*
	*@return true - if the player's card matches the guessed card - false otherwise.
	*
	*/
	public boolean disprove (Player p){
		// This method does not determine which player it is called on

		
		Card[] h = p.hand();
		// This will call hand() on the given player, which should return the 
		// array of cards attached to the player, aka their "hand"
		for (int i=0; i<guess.length; i++){
			Card ca = guess[i];
			// this is the players test card
			cursor = 0;
			// cursor helps track where in the players hand the matching card is found
			for (int j=0; j<h.length; j++){
				Card pca = h[j];
				// this goes through the players hand it is checking against
				if (pca.equals(ca)){
					return true;
				
				}
				cursor ++;
			}
		}
		return false;

		
		
		// It only takes a players hand and checks it against the guess
		Card[] h = p.hand();
		// This will call hand() on the given player, which should return the 
		// array of cards attached to the player, aka their "hand"
		for (int i=0; i<guess.length; i++){
			Card ca = guess[i];
			// this is the players test card
			cursor = 0;
			// cursor helps track where in the players hand the matching card is found
			for (int j=0; j<h.length; j++){
				Card pca = h[j];
				// this goes through the players hand it is checking against
				if (pca.equals(ca)){
					return true;
					// this returns true if the players card matches the guess card
				}
				cursor ++;
			}
		}
		return false;
	}
	
	/**
	*
	* Adds player's final guesses to a "final" array.
	* The player who made the final guess wins the game.
	*
	*/	
	
	public void  finalGuess() {
		ArrayList<Card> final = new Card[3];
		if (final == SecretEnvelope){
			//player who made guess wins game 
		}
		else{
			//player token is removed from game board
			//player can not move only disprove
		}
	}
}

