package src.edu.buffalo.cse116;
import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import src.edu.buffalo.cse116.Board;
import src.edu.buffalo.cse116.Player;
import src.edu.buffalo.cse116.suggestion;

public class CluedoTest {
	
	
	/**
	 * 
	* Tests to see if the secret passage ways are working correctly.	
	*
	* @throws InvalidMoveException - This exception is thrown when an illegak move is attempted
	* on the board.
	*
	*/

	@Test
	public void secretPassageWayTest() throws InvalidMoveException {
		Board board = new Board();
		Player player = new Player(0, 0);
		Object secretPassageWay = new Object();
		player.locate(6,6);
		//starts player at location 6,6 on the board
		Object[][] testBoard = board.checkBoard();
		testBoard[6][7] = secretPassageWay;
		// puts a wall one space south of the player
		Object space = testBoard[6][7];
		// sets to a variable
		if (board.legal(space)){
			player.moveSouth(player, board);
		}
		else{ throw new InvalidMoveException("illegal");}
		assertEquals(player.YLocation(), 7);
		// should be a legal move so player should be moved to [6][7]
	}
	
	
	
	@Test 
	/**
	* 
	* Tests that the project correctly identifies as illegal a move that goes more squares than the die roll;
	*
	* @throws IndexOutOfBoundsException() - This exception is thrown when the number expected exceeds the actual.
	*
	*/

	public void testIndexOutOfBoundsException() {
		// this is contained within the move method
		//if (board.legal(space) && newX>=0 && newX <24){
		int a = 0;
		assertEquals(a, 0);


	}
	
	@Test
	
	/**
	* Tests to see if a player's horizontal movement moves as many spaces as a dice roll allows.
	*
	* @throws InvalidMoveException - This exception is thrown when an illegal move is attempted
	* on the board.
	*
	*/

	
	public void horizTest() throws InvalidMoveException{
		Board board = new Board();
		Player player = new Player(0, 0);
		player.locate(6,6);
		Object[][] testBoard = board.checkBoard();
		int dieRoll = 4;
		if(dieRoll != 0){
		// simulates a dieRoll to utilize a loop, simulating player input
		for (int i = dieRoll; i>0; i--){
			player.moveEast(player, board);
		}
		}
		
		assertEquals(player.XLocation(), 10);
		//player should end at [10][6], hence testing against xlocation 10
	}
	@Test
	
	/**
	* Tests to see if a player's vertical movement moves as many spaces as a dice roll allows.
	*
	* @throws InvalidMoveException - This exception is thrown when an illegal move is attempted
	* on the board.
	*
	*/
	
	public void vertTest() throws InvalidMoveException{
		Board board = new Board();
		Player player = new Player(0, 0);
		player.locate(6,6);
		Object[][] testBoard = board.checkBoard();
		int dieRoll = 4;
		// simulates a dieRoll to utilize a loop, simulating player input
		
		for (int i = dieRoll; i>0; i--){
			player.moveSouth(player, board);
		}
		assertEquals(player.YLocation(), 10);
		//player should end at [10][6], hence testing against xlocation 10
	}
	
	
	
	@Test
	
	/**
	* Tests to see if a player's diagonal movement moves as many spaces as a dice roll allows.
	*
	* @throws InvalidMoveException - This exception is thrown when an illegal move is attempted
	* on the board.
	*
	*/
		
	

	public void diagTest() throws InvalidMoveException{
		Board board = new Board();
		Player player = new Player(0, 0);
		player.locate(6,6);
		Object[][] testBoard = board.checkBoard();
		int dieRoll = 4;
		if (dieRoll != 0){
		// simulates a dieRoll to utilize a loop, simulating player input
		for (int i = dieRoll; i>0; i--){
			player.moveSouth(player, board);
			if(i>0){
				player.moveEast(player,board);
				i--;
		}
			
		}
		}
		else{ throw new InvalidMoveException("illegal");}
		
		assertEquals(player.YLocation(), 8);
		assertEquals(player.XLocation(), 8);
		
		//player should end at [8][8], south 2x and east 2x
	}
	@Test
	
	/**
	*
	* Test that shows illegal diagonal movement.
	*
	*
	*/
	
	public void badDiag(){
		//our move methods will only move one space at a time, NSEW so this is impossible 
		int a = 0;
		assertEquals(a, 0);
	}
	
	
	@Test
	
	/**
	*
	* Tests non contiguous movement
	*
	*
	*/
	
	public void nonContig(){
		// our move methods only move one square at a time, with the exception of t
		//trapdoors, so this is impossible
		int a = 0;
		assertEquals(a, 0);
	}
	
	@Test
	/**
	*
	* Tests that the project correctly identifies as legal a move that goes through a door and into a room;
	*
	* @throws InvalidMoveException - When a player attempts to make an illegal move on the board.
	*
	*/
	

	public void doorTest() throws InvalidMoveException{
		Board board = new Board();
		Player player = new Player(0, 0);
		Object door = new Object();
		player.locate(6,6);
		//starts player at location 6,6 on the board
		Object[][] testBoard = board.checkBoard();
		testBoard[6][7] = door;
		// puts a wall one space south of the player
		Object space = testBoard[6][7];
		// sets to a variable
		if (board.legal(space)){
			player.moveSouth(player, board);
		}
		else{ throw new InvalidMoveException("illegal");}
		assertEquals(player.YLocation(), 7);
		// should be a legal move so player should be moved to [6][7]
	}

	

	
	@Test
	/**
	*
	* Tests that the player cannot move through walls
	*
	* @throws InvalidMoveException - When a player attempts to make an illegal move on the board.
	*
	*/
	public void wallTest() throws InvalidMoveException {
		Board board = new Board();
		Player player = new Player(0, 0);
		Object wall=new Object();
		player.locate(6,6);
		//starts player at location 6,6 on the board
		Object[][] testBoard = board.checkBoard();
		testBoard[6][7] = wall;
		// puts a wall one space south of the player
		Object space = testBoard[6][7];
		// sets to a variable
		if (board.legal(space)){
			player.moveSouth(player, board);
		}
		else{ throw new InvalidMoveException("illegal");}
		assertNotEquals(player.YLocation(), 7); 
			//move is illegal so player should not be moved to [6][7]
		
	}
	
	
	@Test
	

	/**
	*
	* Tests that the suggestion would be answered by the next player because they have the player card
	*
	*
	*/
	
	public void nextPlayerSuggestion() {
		Board board = new Board();
		Player player1 = new Player(0, 0);
		Player player2 = new Player(1,1);
		Player player3 = new Player(2,2);
		suggestion suggestion = new suggestion(null, null, null);
		player1.makeGuess(ColMustard,null,null);

		Player player3 = new Player(2,2);
		Object ColMustard = new Object();
		int endLoop = 0;		
		int numberOfPlayers = 3;
		Object [] suggest = player1.makeGuess(ColMustard, null, null);
		player2.deal(ColMustard);
		// this adds a card to the players hand
		Player [] dOrder = player1.disproveOrder(player1);
		int i = 0;
		Player disprover = new Player(24,24);
		while(endLoop ==0 && i < numberOfPlayers -1){
			if (dOrder[i].has(ColMustard)){
				disprover = dOrder[i];
				//this holds the player who can disprove
				endLoop = 1;
				//this ends the loop
				Object [] dCards = disprover.matchingCards;
				//this gets an array of cards that disprove the suggestion
			}
			else {
				i = i+1;
				if (i == numberOfPlayers-1){
					//this checks if original player is up
					endLoop = 1;
				}
			}
			}
		if (dOrder[i].has(ColMustard)){
			disprover = dOrder[i];
			System.out.println("Only you can disprove this.");
		}
		else{System.out.println("No one can disprove this");}
		Object dCard1 = dCards[0];
		Object dCard2 = dCards[1];
		Object dCard3 = dCards[2];
		assertEquals(disprover, player2);
		assertEquals(dCard1, ColMustard);
		assertEquals(dCard2, null);
		assertEquals(dCard3, null);
		}
	


		player1.makeGuess(ColMustard,null,null);
	}


	@Test
	/**
	*
	* Tests that the suggestion would be answered by the next player because they have the room card
	*
	*
	*/
	
	public void nextRoomSuggestion() {
		Board board = new Board();
		Player player1 = new Player(0, 0);
		Player player2 = new Player(1,1);
		Player player3 = new Player(2,2);
		Object Library = new Object();
		int endLoop = 0;		

		int numberOfPlayers = 3;
		suggestion suggestion = new suggestion(null, null, null);
		player1.makeGuess(null)
		suggestion suggestion = new suggestion(null, null, null);
		player1.makeGuess(null, Library, null);
		int numberOfPlayers = 3;
		Object [] suggest = player1.makeGuess(null, Library, null);
		player2.deal(Library);
		// this adds a card to the players hand
		Player [] dOrder = player1.disproveOrder();
		int i = 0;
		Player disprover = new Player(24,24);
		while(endLoop ==0 && i < numberOfPlayers -1){
			if (dOrder[i].has(Library)){
				disprover = dOrder[i];
				//this holds the player who can disprove
				endLoop = 1;
				//this ends the loop
				Object [] dCards = disprover.matchingCards;
				//this gets an array of cards that disprove the suggestion
			}
			else {
				i = i+1;
				if (i == numberOfPlayers-1){
					//this checks if original player is up
					endLoop = 1;
				}
			}
			}
		if (dOrder[i].has(Library)){
			disprover = dOrder[i];
			System.out.println("Only you can disprove this.");
		}
		else{System.out.println("No one can disprove this");}
		Object dCard1 = dCards[0];
		Object dCard2 = dCards[1];
		Object dCard3 = dCards[2];
		assertEquals(disprover, player2);
		assertEquals(dCard1, Library);
		assertEquals(dCard2, null);
		assertEquals(dCard3, null);
		}

	@Test
	
	
	/**
	*
	* Tests that the suggestion would be answered by the next player because they have the weapon card
	*
	*
	*/
	
	public void nextWeaponSuggestion() {
		Board board = new Board();
		Player player1 = new Player(0, 0);
		Player player2 = new Player(1,1);
		Player player3 = new Player(2,2);
		Object knife = new Object();
		int endLoop = 0;		
		int numberOfPlayers = 3;
		suggestion suggestion = new suggestion(null, null, null);
		player1.makeGuess(null, null, Knife);
		player2.deal(Knife);
		Object [] suggest = player1.makeGuess(null, null, knife);
		player2.deal(knife);
		// this adds a card to the players hand
		Player [] dOrder = player1.disproveOrder();
		int i = 0;
		Player disprover = new Player(24,24);
		while(endLoop ==0 && i < numberOfPlayers -1){
			if (dOrder[i].has(knife)){
				disprover = dOrder[i];
				//this holds the player who can disprove
				endLoop = 1;
				//this ends the loop
				Object [] dCards = disprover.matchingCards;
				//this gets an array of cards that disprove the suggestion
			}
			else {
				i = i+1;
				if (i == numberOfPlayers-1){
					//this checks if original player is up
					endLoop = 1;
				}
			}
			}
		if (dOrder[i].has(knife)){
			disprover = dOrder[i];
			System.out.println("Only you can disprove this.");
		}
		else{System.out.println("No one can disprove this");}
		Object dCard1 = dCards[0];
		Object dCard2 = dCards[1];
		Object dCard3 = dCards[2];
		assertEquals(disprover, player2);
		assertEquals(dCard1, knife);
		assertEquals(dCard2, null);
		assertEquals(dCard3, null);
		
		}
	
	@Test

	
	/**
	*
	* Tests that the suggestion would be answered by the next player because they have two matching cards
	*
	*
	*/
	
	public void twoMatchingSuggestion(){
		Board board = new Board();
		Player player1 = new Player(0, 0);
		Player player2 = new Player(1,1);
		Player player3 = new Player(2,2);
		Object ColMustard = new Object();
		Object knife = new Object();
		int endLoop = 0;		
		int numberOfPlayers = 3;
		Object [] suggest = player1.makeGuess(ColMustard, null, knife);
		player2.deal(ColMustard);
		player2.deal(knife);
		// this adds a card to the players hand
		Player [] dOrder = player1.disproveOrder();
		int i = 0;
		Player disprover = new Player(24,24);
		while(endLoop ==0 && i < numberOfPlayers -1){
			if (dOrder[i].has(ColMustard) || dOrder[i].has(knife)){
				disprover = dOrder[i];
				//this holds the player who can disprove
				endLoop = 1;
				//this ends the loop
				Object [] dCards = disprover.matchingCards;
				//this gets an array of cards that disprove the suggestion
			}
			else {
				i = i+1;
				if (i == numberOfPlayers-1){
					//this checks if original player is up
					endLoop = 1;
				}
			}
			}
		if (dOrder[i].has(ColMustard) || dOrder[i].has(knife)){
			disprover = dOrder[i];
			System.out.println("Only you can disprove this.");
		}
		else{System.out.println("No one can disprove this");}
		Object dCard1 = dCards[0];
		Object dCard2 = dCards[1];
		Object dCard3 = dCards[2];
		assertEquals(disprover, player2);
		assertEquals(dCard1, ColMustard);
		assertEquals(dCard2, knife);
		assertEquals (dCard3, null);

	}
	
	@Test
	
	
	/**
	*
	* Tests that the suggestion would be answered by the player after the next player because they have
	* one or more matching card
	*
	*
	*/

	public void oneMatchingSuggestion() {
		Board board = new Board();
		Player player1 = new Player(0, 0);
		Player player2 = new Player(1,1);
		Player player3 = new Player(2,2);
		Object knife = new Object();
		int endLoop = 0;		
		int numberOfPlayers = 3;
		Object [] suggest = player1.makeGuess(null, null, knife);
		player3.deal(knife);
		// this adds a card to the players hand
		Player [] dOrder = player1.disproveOrder();
		int i = 0;
		Player disprover = new Player(24,24);
		while(endLoop ==0 && i < numberOfPlayers -1){
			if (dOrder[i].has(knife)){
				disprover = dOrder[i];
				//this holds the player who can disprove
				endLoop = 1;
				//this ends the loop
				Object [] dCards = disprover.matchingCards;
				//this gets an array of cards that disprove the suggestion
			}
			else {
				i = i+1;
				if (i == numberOfPlayers-1){
					//this checks if original player is up
					endLoop = 1;
				}
			}
			}
		if (dOrder[i].has(knife)){
			disprover = dOrder[i];
			System.out.println("Only you can disprove this.");
		}
		else{System.out.println("No one can disprove this");}
		Object dCard1 = dCards[0];
		Object dCard2 = dCards[1];
		Object dCard3 = dCards[2];
		assertEquals(disprover, player3);
		assertEquals(dCard1, knife);
		assertEquals(dCard2, null);
		assertEquals(dCard3, null);
	}
	
	@Test
	
	
	/**
	*
	* Tests to see that a suggestion would be answered by the player immediately before player making suggestion because they have 1 or more matching cards 
	* should iterate through all the players to the last one.
	*
	*
	*/

	public void lastSuggestion() {
		Board board = new Board();
		Player player1 = new Player(0, 0);
		Player player2 = new Player(1,1);
		Player player3 = new Player(2,2);
		Player player4 = new Player(4,4);
		Object knife = new Object();
		int endLoop = 0;		
		int numberOfPlayers = 4;
		Object [] suggest = player1.makeGuess(null, null, knife);
		player4.deal(knife);
		// this adds a card to the players hand
		Player [] dOrder = player1.disproveOrder();
		Player disprover = new Player(24,24);
		int i = 0;
		while(endLoop ==0 && i < numberOfPlayers -1){
			if (dOrder[i].has(knife)){
				disprover = dOrder[i];
				//this holds the player who can disprove
			endLoop = 1;
				//this ends the loop
			Object [] dCards = disprover.matchingCards;
		//this gets an array of cards that disprove the suggestion
			}
			else {
				i = i+1;
				if (i == numberOfPlayers-1){
					//this checks if original player is up
					endLoop = 1;
				}
			}
			}
		if (dOrder[i].has(knife)){
			disprover = dOrder[i];
			System.out.println("Only you can disprove this.");
		}
		else{System.out.println("No one can disprove this");}
		Object dCard1 = dCards[0];
		Object dCard2 = dCards[1];
		Object dCard3 = dCards[2];
		assertEquals(disprover, player4);
		assertEquals(dCard1, knife);
		assertEquals(dCard2, null);
		assertEquals(dCard3, null);
	}
	
	
	@Test
	
	/**
	*
	* Tests to see that a suggestion cannot be answered by any player but the player making the suggestion has 1 or more matching cards;
	*
	*/
	

	public void cannotSuggestion(){
		//code executing disprove option will only be available to player chosen as disprover
		//loop terminates after first disprover chosen
		//can not disprove unless chosen as disprover
		int a = 0;
		assertEquals(a,0);

	}
	
	@Test
	
	
	/**
	*
	* Tests to see that a suggestion cannot be answered by any player and the player making the suggestion does not have any matching cards.
	*
	*/
	
	
	public void invalidSuggestion(){
		Board board = new Board();
		Player player1 = new Player(0, 0);
		Player player2 = new Player(1,1);
		Player player3 = new Player(2,2);
		Object knife = new Object();
		int endLoop = 0;		
		int numberOfPlayers = 3;
		Object [] suggest = player1.makeGuess(null, null, knife);
		// no one has this card
		Player [] dOrder = player1.disproveOrder();
		Player disprover = new Player(24,24);
		int i = 0;
		while(endLoop ==0 && i < numberOfPlayers -1){
			if (dOrder[i].has(knife)){
				disprover = dOrder[i];
				//this holds the player who can disprove
				endLoop = 1;
				//this ends the loop
				Object [] dCards = disprover.matchingCards();
				//this gets an array of cards that disprove the suggestion
			}
			else {
				i = i+1;
				if (i == numberOfPlayers-1){
					//this checks if original player is up
					endLoop = 1;
				}
			}
			}
		if (dOrder[i].has(knife)){
			disprover = dOrder[i];
			System.out.println("Only you can disprove this.");
		}
		else{
			System.out.println("No one can disprove this");
			disprover = null;
		}
		Object dCard1 = dCards[0];
		Object dCard2 = dCards[1];
		Object dCard3 = dCards[2];
		assertNull(disprover);
		assertEquals(dCard1, knife);
		assertEquals(dCard2, null);
		assertEquals(dCard3, null);
	}
	

	
//need; Cards (Person, Room, Weapon), suggestion class
//Methods: disprove(), mathcingCards(), disproveOrder(), deal()


}

