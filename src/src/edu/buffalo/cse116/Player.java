package src.edu.buffalo.cse116;

public class Player {
	
	private String characterName;
	private int _X;
	private int _Y;
	// These track player location on the Board
	private int player;
	private Object [] hand;
	private int cursor;
	//this is for tracking cards in the hand
	
	
	
	
	
	/**
	* Simply sets up x and y coordinates for players on the board. 
	*
	*
	*@pram x - Track's the characters X coordinate on the 2D array board
	*@param y - Track's the characters Y coordinates on the 2D array board
	*
	*/	
	public Player(int x, int y){
		_X = x;
		_Y = y;
		hand = new Object[16];
		cursor = 0;
	}
	
	
	public int XLocation(){
		int getX = _X;
		return getX;

}
	public int YLocation(){
		int getY = _Y;
		return getY;
}
	public void locate(int a, int b){
		_X = a;
		_Y = b;
	}
	
	public String getCharacterName() {
		return characterName;
	}
	
	
	

	/**
	*
	*Attemps to move the player north on the board. Cannot move through walls.
	*
	*@param player- the player that is moving
	*
	*@param board - the 2D board in which the player moves through
	*
	*@throws InvalidMoveException- Thrown when a player attemps to make an illegal move.
	*
	*/	
	public void moveNorth(Player player, Board board) throws InvalidMoveException{
		int newY = player.YLocation() -1;
		Object [][] store = board.checkBoard();
		Object space = store[player.XLocation()][newY];
		if (board.legal(space) && newY>=0 && newY <24){
			//checks location player wants to move to
			player.locate(player.XLocation(), newY);
		}
		else { throw new InvalidMoveException("illegal");}
		//need to write this exception, should return a string and rerun move choice
		
	}
	
	
	/**
	*
	*Attemps to move the player west on the board. Cannot move through walls.
	*
	*@param player- the player that is moving
	*
	*@param board - the 2D board in which the player moves through
	*
	*@throws InvalidMoveException- Thrown when a player attemps to make an illegal move.
	*
	*/	
	public void moveWest(Player player, Board board) throws InvalidMoveException{
		int newX = player.XLocation() -1;
		Object [][] store = board.checkBoard();
		Object space = store[newX][player.YLocation()];
		if (board.legal(space) && newX>=0 && newX <24){
			//checks location player wants to move to
			player.locate(newX, player.YLocation());
			
		}
		else { throw new InvalidMoveException("illegal");}
		//need to write this exception, should return a string and rerun move choice
		
	}
	
	/**
	*
	*Attemps to move the player south on the board. Cannot move through walls.
	*
	*@param player- the player that is moving
	*
	*@param board - the 2D board in which the player moves through
	*
	*@throws InvalidMoveException- Thrown when a player attemps to make an illegal move.
	*
	*/	
	
	public void moveSouth(Player player, Board board) throws InvalidMoveException{
		int newY = player.YLocation() +1;
		Object [][] store = board.checkBoard();
		Object space = store[player.XLocation()][newY];
		if (board.legal(space) && newY>=0 && newY <24){
			//checks location player wants to move to
			player.locate(player.XLocation(), newY);
		}
		else { throw new InvalidMoveException("illegal");}
		//need to write this exception, should return a string and rerun move choice
		
	}
	
	/**
	*
	*Attemps to move the player east on the board. Cannot move through walls.
	*
	*@param player- the player that is moving
	*
	*@param board - the 2D board in which the player moves through
	*
	*@throws InvalidMoveException- Thrown when a player attemps to make an illegal move.
	*
	*/	
	public void moveEast(Player player, Board board) throws InvalidMoveException{
		int newX = player.XLocation() -1;
		Object [][] store = board.checkBoard();
		Object space = store[newX][player.YLocation()];
		if (board.legal(space) && newX>=0 && newX <24){
			//checks location player wants to move to
			player.locate(newX, player.YLocation());
		}
		else { throw new InvalidMoveException("illegal");}
		//need to write this exception, should return a string and rerun move choice
		
	}
	
	
	/**
	*Iterates through a player's hands.
	*
	*@return true- If the selected card is in the players' hand. - false otherwise.
	*
	*@param o- An object referencing a card.
	*
	*
	*/	
	public boolean has(Object o){
		for (int i=0; i<hand.length; i++){
			  if (hand[i] == o){
				  return true;
			  }
			  else if(hand[i] != null && hand[i].equals(o)){
				  return true;
			  }
		  }
		  return false;
		}
	
	
	
	/**
	*
	*The player will attempt to make a guess.
	*
	*@return guess- Returns the guess of a combination of a person, room and weapon cards.
	*
	*@param p- An object referencing a person card.
	*
	*@param r- An object referencing a room card
	*
	*@param w- An object referencing a weapon card
	*
	*
	*/	
	
	public Object[] makeGuess(Object p, Object r, Object w) {
		Object [] guess = new Object [3];
		guess[0] = p;
		guess[1] = r;
		guess[2] = w;
		return guess;
	}

	public Player[] disproveOrder() {
		
		return null;
	}
	
	
	
	/**
	*
	*Deals a card to a player's hand.
	*
	*
	*@param o- An object referencing a card.
	*
	*/	

	public void deal(Object o){
		hand[cursor] = o;
		cursor++;
	}
}