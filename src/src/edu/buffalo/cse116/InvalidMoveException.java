package src.edu.buffalo.cse116;

public class InvalidMoveException extends Exception {
	

	/**
	*
	*InvalidMoveException- Thrown when a player makes an illegal move on the board.
	*
	*@param message - The message shown when this exception is thrown
	*
	*/

	private static final long serialVersionUID = 1L;
	public InvalidMoveException(String message){
		super(message);
	}

}
